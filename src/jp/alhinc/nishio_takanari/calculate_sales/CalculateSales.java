package jp.alhinc.nishio_takanari.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
//		コマンドライン引数が渡されているか確認
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String,String> branchNames = new HashMap<>();
		Map<String,Long> branchSales = new HashMap<>();
//		支店定義ファイル（branch.lst）入力メソッド使用
		if(!branchFileRead(args[0], branchNames, branchSales)) {
			return;
		}

		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();
//		商品定義ファイル(commdity.lst)入力メソッド使用
		if(!commodityFileRead(args[0], commodityNames, commoditySales)) {
			return;
		}


//		売上ファイルの入力準備
//		指定パス内のフォルダ＆ファイルの配列生成
		File[] files = new File(args[0]).listFiles();

//		売上ファイルのみのList生成
//		指定のパスに存在するものの名前が正規表現にマッチ && ファイル形式 = 売上ファイル
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^\\d{8}\\.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

//		売上ファイルListに格納されるファイル名が連番であるかの確認
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

//		配列rcdFiles内の売上ファイルの入力
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader bf = null;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				bf = new BufferedReader(fr);
				List<String> salesInfo = new ArrayList<>();

				String line;
				while((line = bf.readLine()) != null) {
					salesInfo.add(line);
				}

//				売上ファイルフォーマット確認
				if(salesInfo.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

//				売上ファイルの支店コードが既存のものか
				if(!branchNames.containsKey(salesInfo.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

//				売上ファイルの商品コードが既存のものか
				if(!commodityNames.containsKey(salesInfo.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

//				売上ファイルの金額(salesInfo.get(2))が数字か確認
				if(!salesInfo.get(2).matches("^\\d+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

//				branchSales/commoditySalesに使用する金額の集計
				long fileSale = Long.parseLong(salesInfo.get(2));
				Long salesAmountBranch = branchSales.get(salesInfo.get(0)) + fileSale;
				Long salesAmountCommodity = commoditySales.get(salesInfo.get(1)) + fileSale;

//				集計結果の金額不備確認
				if(salesAmountBranch >= 10000000000L || salesAmountCommodity >= 10000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

//				branchSales/commoditySalesのvalue(金額)更新
				branchSales.replace(salesInfo.get(0), salesAmountBranch);
				commoditySales.replace(salesInfo.get(1), salesAmountCommodity);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(bf != null) {
					try {
						bf.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

//		支店別集計ファイル出力
		if(!fileWrite(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

//		商品別集計ファイル出力
		if(!fileWrite(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}

//	支店定義ファイル入力メソッド
	private static boolean branchFileRead(String filePath, Map<String, String> namesMap, Map<String, Long> salesMap) {
		BufferedReader br = null;
		try {
			File file = new File(filePath, "branch.lst");

//			指定のファイルが存在するか確認
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if((items.length != 2) || !(items[0].matches("^\\d{3}$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				String branchCode = items[0];
				String branchName = items[1];
				namesMap.put(branchCode, branchName);
				salesMap.put(branchCode, 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

//	商品定義ファイル入力メソッド
	private static boolean commodityFileRead(String filePath, Map<String, String> namesMap, Map<String, Long> salesMap) {
		BufferedReader br = null;
		try {
			File file = new File(filePath, "commodity.lst");

//			ファイルの有無確認
			if(!file.exists()) {
				System.out.println("商品定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

//				ファイルのフォーマット確認
				if((items.length != 2) || !(items[0].matches("^[a-zA-Z0-9]{8}$"))) {
					System.out.println("商品定義ファイルのフォーマットが不正です");
					return false;
				}

				String commodityCode = items[0];
				String commodityName = items[1];
				namesMap.put(commodityCode, commodityName);
				salesMap.put(commodityCode, 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

//	共通ファイル出力メソッド
	private static boolean fileWrite(String filePath, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {
		BufferedWriter bw = null;
		try {
			File file = new File(filePath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key: namesMap.keySet()) {
				String line = key + "," + namesMap.get(key) + "," + salesMap.get(key);
				bw.write(line);
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

			}
		}
		return true;
	}
}
